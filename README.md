# Wolox Challenge.
Proyecto desarrollado en Node.Js con el framework Express para dar solución al ejercicio técnico `Cryptocurrencies Monitor`.

## Inicialización del proyecto.
1. npm install.

## Comandos de ejecución.
1. npm run ts -> comando para compilar los archivos typescript.
2. npm run start -> comando para correr el servidor.
3. npm run test -> comando para ejecutar las pruebas unitarias.
4. npm run dev -> comando para correr el servidor (Nodemon).

## Gestor de datos.
1. [MongoDB](https://docs.mongodb.com/).

## Variables de entorno.
1. `SEED`: Es utilizada para la firma de los JSON Web Tokens.
2. `MONGODB_URL`: Url de la base de datos, de no ser inicializada se crea la siguiente base de datos (`mongodb://localhost:27017/localCryptoCurrencies`).

### Postman Collection
1. En la carpeta principal se encuentra el archivo `Desafio Wolox.postman_collection` que contiene todas las peticiones desarrolladas en el aplicativo.

### Contacto LinkedIn
[Juan M Sánchez](https://www.linkedin.com/in/juan-sanchez-3005/).
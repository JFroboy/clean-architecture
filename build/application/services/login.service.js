"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_script_1 = __importDefault(require("../../infrastructure/database/scripts/user.script"));
var login_interactor_1 = require("../interactors/login/login.interactor");
var userRepository = new user_script_1.default();
exports.default = login_interactor_1.loginUser(userRepository);

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_script_1 = __importDefault(require("../../infrastructure/database/scripts/user.script"));
var user_interactor_1 = require("../interactors/user/user.interactor");
var userRepository = new user_script_1.default();
exports.default = user_interactor_1.createUser(userRepository);

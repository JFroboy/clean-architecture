"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.URL_COIN_GECKO = exports.TOKEN_EXPIRES = void 0;
exports.TOKEN_EXPIRES = 60 * 60;
exports.URL_COIN_GECKO = 'https://api.coingecko.com/api/v3';

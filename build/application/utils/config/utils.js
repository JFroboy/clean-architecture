"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkCurrency = exports.encryptar = void 0;
var bcrypt_1 = __importDefault(require("bcrypt"));
var encryptar = function (password) {
    var newPassword = bcrypt_1.default.hashSync(password, 12);
    return newPassword;
};
exports.encryptar = encryptar;
var checkCurrency = function (currency) {
    var listCurrencies = ['usd', 'eur', 'ars'];
    var response = (listCurrencies.includes(currency)) ? true : false;
    return response;
};
exports.checkCurrency = checkCurrency;

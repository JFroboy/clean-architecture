"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyToken = void 0;
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var verifyToken = function (req, res, next) {
    var _a;
    var token = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(" ")[1];
    var seed = process.env.SEED;
    jsonwebtoken_1.default.verify(token, seed, function (err, decoded) {
        if (err) {
            return res.status(401).json({ Status: false, Message: err.message, Data: null });
        }
        req.headers.user = decoded;
        next();
    });
};
exports.verifyToken = verifyToken;

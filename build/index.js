"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
var dotenv_1 = __importDefault(require("dotenv"));
var conexion_1 = require("./infrastructure/database/conexion");
var user_routes_1 = __importDefault(require("./infrastructure/routes/user.routes"));
var login_routes_1 = __importDefault(require("./infrastructure/routes/login.routes"));
var Server = /** @class */ (function () {
    function Server() {
        dotenv_1.default.config();
        this.app = express_1.default();
        this.config();
        this.routes();
        new conexion_1.Conexion().connect();
    }
    Server.prototype.config = function () {
        /** Settings */
        this.app.set('port', process.env.PORT || 8000);
        this.app.set('json spaces', 2);
        /** Middlewares */
        //dev - combined : Shows detailed information of the requests made to the server
        this.app.use(morgan_1.default('dev'));
        // Allows the server to understand the JSON format
        this.app.use(express_1.default.json());
    };
    Server.prototype.routes = function () {
        this.app.use('/api/login', login_routes_1.default);
        this.app.use('/api/user', user_routes_1.default);
        this.app.get('/', function (req, res) {
            res.send('Hello Wolox!.');
        });
    };
    Server.prototype.start = function () {
        var _this = this;
        this.app.listen(this.app.get('port'), function () {
            console.log("Server on port " + _this.app.get('port'));
        });
    };
    return Server;
}());
var server = new Server();
server.start();

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var login_controller_1 = require("../controllers/login.controller");
var LoginRoutes = /** @class */ (function () {
    function LoginRoutes() {
        this.router = express_1.Router();
        this.routes();
    }
    LoginRoutes.prototype.routes = function () {
        this.router.post('/user', login_controller_1.loginUserController);
    };
    return LoginRoutes;
}());
var loginRoutes = new LoginRoutes();
exports.default = loginRoutes.router;

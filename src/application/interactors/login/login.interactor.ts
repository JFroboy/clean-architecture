import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import UserRepository from '../../../domain/repositories/user.repository';
import * as constants from '../../utils/config/constants';

export const loginUser = (userRepository: UserRepository) => async (userName: string, passWord: string) => {
    return new Promise<string>(async (resolver, reject) => {
        try {
            const user = await userRepository.getUserByUsername(userName);
            if (user) {
                if (!bcrypt.compareSync(passWord, user.password)) {
                    return reject('Las credenciales ingresadas son incorrectas');
                } else {
                    const seed: any = process.env.SEED;
                    const token = jwt.sign({
                        _id: user._id, user_name: user.user_name, preferred_currency: user.preferred_currency
                    }, seed, { expiresIn: constants.TOKEN_EXPIRES });
                    resolver(token)
                }
            } else {
                reject('Las credenciales ingresadas son incorrectas');
            }
        } catch (err) {
            reject(`${err}`);
        }
    });
}

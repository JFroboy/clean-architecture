import User from '../../../domain/entities/User';
import UserRepository from '../../../domain/repositories/user.repository';
import { checkCurrency, encryptar } from '../../utils/config/utils'

export const createUser = (userRepository: UserRepository) => async (data: User) => {
    return new Promise<User>(async (resolve, reject) => {
        try {
            data.preferred_currency = data.preferred_currency.toLowerCase();
            if (!checkCurrency(data.preferred_currency)) return reject('Verifica la divisa seleccionada');
            if (data.password.length < 8) return reject('La contraseña debe tener minimo 8 caracteres');
            data.password = encryptar(data.password);
            const newUser: User = await userRepository.createUser(data);
            resolve(newUser)
        } catch (err) {
            reject(`${err}`)
        }
    });
}

import UserScript from '../../infrastructure/database/scripts/user.script';
import { loginUser } from '../interactors/login/login.interactor';

const userRepository = new UserScript();

export default loginUser(userRepository);

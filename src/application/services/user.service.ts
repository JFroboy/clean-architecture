import UserScript from '../../infrastructure/database/scripts/user.script';
import { createUser } from '../interactors/user/user.interactor';

const userRepository = new UserScript();

export default createUser(userRepository);
import bcrypt from 'bcrypt'

export const encryptar = (password: string) => {
    const newPassword: string = bcrypt.hashSync(password, 12);
    return newPassword
}

export const checkCurrency = (currency: string) => {
    const listCurrencies: string[] = ['usd', 'eur', 'ars'];
    let response: boolean = (listCurrencies.includes(currency)) ? true : false;
    return response
}

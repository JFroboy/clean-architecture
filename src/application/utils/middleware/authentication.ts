import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';

export const verifyToken = (req: Request, res: Response, next: any) => {
    const token: any = req.headers.authorization?.split(" ")[1];
    const seed: any = process.env.SEED;

    jwt.verify(token, seed, (err: any, decoded: any) => {
        if (err) {
            return res.status(401).json({ Status: false, Message: err.message, Data: null });
        }
        req.headers.user = decoded;
        next();
    })
}
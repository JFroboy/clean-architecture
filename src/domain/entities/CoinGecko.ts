export default interface CoinGecko {
    id: string;
    symbol: string;
    current_price: number;
    name: string;
    image: string;
    last_updated: Date;
}

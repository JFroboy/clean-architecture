export default interface User {
    _id?: string;
    name: string;
    last_name: string;
    user_name: string;
    password: string;
    preferred_currency: string;
}

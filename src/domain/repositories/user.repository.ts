import User from '../entities/User';

interface UserRepository {
    getUserByUsername(user_name: string): Promise<User>
    createUser(data: User): Promise<User>
}

export default UserRepository

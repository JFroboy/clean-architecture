import express from "express";
import morgan from "morgan";
import dotenv from "dotenv";
import { Conexion } from "./infrastructure/database/conexion";

import userRoutes from "./infrastructure/routes/user.routes";
import loginRoutes from './infrastructure/routes/login.routes';

class Server {
    public app: express.Application;

    constructor() {
        dotenv.config();
        this.app = express();
        this.config();
        this.routes();
        new Conexion().connect();
    }

    config() {
        /** Settings */
        this.app.set('port', process.env.PORT || 8000)
        this.app.set('json spaces', 2)

        /** Middlewares */
        //dev - combined : Shows detailed information of the requests made to the server
        this.app.use(morgan('dev'));
        // Allows the server to understand the JSON format
        this.app.use(express.json());

    }

    routes() {
        this.app.use('/api/login', loginRoutes);
        this.app.use('/api/user', userRoutes);
        this.app.get('/', (req, res) => {
            res.send('Hello Wolox!.')
        });
    }

    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log(`Server on port ${this.app.get('port')}`);
        });

    }
}

const server = new Server();
server.start();
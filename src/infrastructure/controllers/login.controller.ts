import { Request, Response } from 'express';
import loginUser from '../../application/services/login.service';

export const loginUserController = async (req: Request, res: Response) => {
    try {
        const { user_name, password } = req.body;
        await loginUser(user_name, password).then((token: string) => {
            res.status(200).json({ Status: true, Message: '', Data: token });
        }).catch((err) => {
            res.status(400).json({ Status: false, Message: err, Data: null });
        });
    } catch (err) {
        res.status(500).json({ Status: false, Message: err, Data: null });
    }
}

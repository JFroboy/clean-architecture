import { Request, Response } from 'express';
import createUser from '../../application/services/user.service';

export const createUserController = async (req: Request, res: Response) => {
    try {
        const { body } = req;
        await createUser(body).then((user: any) => {
            res.status(200).json({ Status: true, Message: '', Data: user });
        }).catch((err) => { res.status(400).json({ Status: false, Message: err, Data: null }); })
    } catch (err) {
        res.status(500).json({ Status: false, Message: err, Data: null });
    }
}

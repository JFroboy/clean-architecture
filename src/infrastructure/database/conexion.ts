import mongoose from "mongoose";
import dotenv from 'dotenv';

//****************************************/

export class Conexion {
    public MONGO_URL: string;

    constructor() {
        dotenv.config();
        this.MONGO_URL = process.env.MONGODB_URL || 'mongodb://localhost:27017/localCryptoCurrencies';
    }

    async connect() {
        if (mongoose.connection.readyState === 0) {
            mongoose.set('useFindAndModify', true);
            mongoose.connect(this.MONGO_URL, {
                useNewUrlParser: true,
                useCreateIndex: true,
                useUnifiedTopology: true
            }).then(db => console.log('db is connected: ' + this.MONGO_URL))
                .catch(err => console.log(`db is not connected: ${err}`));
        }
    };

    async disconnect() {
        if (mongoose.connection.readyState !== 0) {
            await mongoose.disconnect();
        }
    };

}

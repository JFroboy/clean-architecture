import { Schema, model } from 'mongoose';

const UserSchema = new Schema({
    name: { type: String, required: [true, "El nombre es requerido"] },
    last_name: { type: String, required: [true, "El apellido es requerido"] },
    user_name: { type: String, unique: true, required: [true, "El usuario es requerido"] },
    password: { type: String, required: [true, "La contraseña es requerida"] },
    preferred_currency: { type: String, required: [true, "La moneda es requerida"] },
}, {
    timestamps: true
});

export default model('User', UserSchema);

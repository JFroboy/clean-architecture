import User from "../../../domain/entities/User";
import UserRepository from "../../../domain/repositories/user.repository";
import UserSchema from '../models/user';

class UserScript implements UserRepository {
    getUserByUsername(user_name: string): Promise<User> {
        return new Promise<User>(async (resolve, reject) => {
            try {
                await UserSchema.findOne({ user_name }).exec(async (err: any, user: any) => {
                    if (err) return reject(err.message);
                    resolve(user)
                })
            } catch (err) {
                reject(err)
            }
        });
    }

    createUser(data: User): Promise<User> {
        return new Promise<User>(async (resolve, reject) => {
            try {
                const newUser = new UserSchema(data);
                newUser.save(function (err: any, user: User) {
                    if (err) reject(err.message);
                    resolve(user)
                });
            } catch (err) {
                reject(err)
            }
        });
    }
}

export default UserScript
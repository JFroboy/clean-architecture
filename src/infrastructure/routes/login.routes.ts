import { Router } from 'express';
import { loginUserController } from '../controllers/login.controller';

class LoginRoutes {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    routes() {
        this.router.post('/user', loginUserController);
    }
}

const loginRoutes = new LoginRoutes();
export default loginRoutes.router;

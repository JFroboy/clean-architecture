import { Router } from 'express';
import { createUserController } from '../controllers/user.controller';

class UserRoutes {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    routes() {
        this.router.post('/', createUserController);
    }
}

const userRoutes = new UserRoutes();
export default userRoutes.router;
